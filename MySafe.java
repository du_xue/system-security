package Mysafe;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MySafe {
	private static String password = null;
	private Key keyA = null;
	private Key keyB = null;
	private static byte[] keyAES = null;
	private static byte[] keyMAC = null;
	byte[] rand = null;
	private static byte[] MAC = null;
	private byte[] result = null;
	
	private String assert1 = null;
	private String assert2 = null;
	
	private static IvParameterSpec iv = null;
public byte[] encrypt(String password1, byte[] plainText ) throws NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
	password = password1;
	
	byte[] srcBytes = null;
	MessageDigest md = MessageDigest.getInstance("SHA-256");// generate the keys fron the password
	srcBytes = password.getBytes("UTF-8");  // change the password to bytes.
	md.update(srcBytes);
	byte[] fullPassword = md.digest();// genenrate the 32-byte key
	System.out.println("fullpassword= "+new String(fullPassword, "UTF-8")+"d = "+fullPassword.length);
	keyAES = Arrays.copyOfRange(fullPassword, 0, 16);// seperate the 32-byte, this is the first part
	keyMAC = Arrays.copyOfRange(fullPassword, 16, 32);//seperate the 32-byte, this is the sencond part
	//System.out.println("= "+fullPassword+" length "+fullPassword.length );
	//System.out.println("keyAES= "+new String(keyAES, "UTF-8")+"d = "+keyAES.length);
	//System.out.println("keyMAC= "+new String(keyMAC, "UTF-8")+"d = "+keyMAC.length);
	SecretKeySpec key1 = new SecretKeySpec(keyAES, "AES");
	SecretKeySpec key2 = new SecretKeySpec(keyMAC, "AES");
	keyA = key1;
	keyB = key2;
	System.out.println("key1= "+new String(key1.getEncoded())+"d = "+keyAES.length);
	System.out.println("key2= "+new String(key2.getEncoded())+"d = "+keyMAC.length);
	
	rand = new byte[16]; 
	Random r = new Random(); 
	r.nextBytes(rand);
	iv = new IvParameterSpec(rand);
	
	Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
	cipher.init(Cipher.ENCRYPT_MODE, key1,iv);
	
    byte[] AES = cipher.doFinal(plainText);
	result = new byte[AES.length + rand.length];
	System.arraycopy(rand, 0, result, 0, rand.length);
	System.arraycopy(AES, 0, result, rand.length, AES.length);
	System.out.println("Result = "+new String(result)+'\n'+"length = "+result.length);
	   
	//MAC
	
    Mac mac = Mac.getInstance("HMACSHA1");
	mac.init(key2);
	MAC = mac.doFinal(result);
    //System.out.println(new String(MAC)+"length = "+MAC.length);
    byte[] outPut = new byte[result.length+ MAC.length];
    System.arraycopy(result, 0, outPut, 0, result.length);
	System.arraycopy(MAC, 0, outPut, result.length, MAC.length);
	System.out.println(new String(outPut)+"length = "+outPut.length);
    return outPut;
    
}
public byte[] decrypt(String password2, byte[] cipherText) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
	password = password2;
	System.out.println("CipherText decrption= "+ new String(cipherText, "UTF-8")+"length= "+cipherText.length);
	
	byte[] srcBytes = null;
	byte[] cipher = Arrays.copyOfRange(cipherText, 16, 32);
	byte[] IV = Arrays.copyOfRange(cipherText, 0, 16);
	byte[] checkMac = Arrays.copyOfRange(cipherText,32, 52);
	byte[] countMac = Arrays.copyOfRange(cipherText,0, 32);
	
	MessageDigest md = MessageDigest.getInstance("SHA-256");
	srcBytes = password.getBytes("UTF-8");
	md.update(srcBytes);
	byte[] fullPassword = md.digest();
	keyAES = Arrays.copyOfRange(fullPassword, 0, 16);
	keyMAC = Arrays.copyOfRange(fullPassword, 16, 32);
	//System.out.println("checkMAC= "+new String(checkMac)+" length =  "+checkMac.length );
	//System.out.println("keyAES= "+new String(keyAES)+"d = "+keyAES.length);
	//System.out.println("keyMAC= "+new String(keyMAC)+"d = "+keyMAC.length);
	//System.out.println(" countMAC = "+new String(countMac)+'\n'+"length = "+countMac.length);
	SecretKeySpec key1 = new SecretKeySpec(keyAES, "AES");
	SecretKeySpec key2 = new SecretKeySpec(keyMAC, "AES");
	
	if(key1.equals(keyA)&&key2.equals(keyB)){
		
		
	    Mac mac = Mac.getInstance("HMACSHA1");
		mac.init(key2);
		byte[] newResult = mac.doFinal(countMac);
        //System.out.println("CipherText decrption= "+ cipherText);
        //System.out.println(" New MAC = "+new String(newResult)+'\n'+"length = "+newResult.length);
        if(Arrays.equals(MAC, newResult)){
        	iv = new IvParameterSpec(IV);
        	Cipher cipher1 = Cipher.getInstance("AES/CBC/PKCS5Padding");
        	cipher1.init(Cipher.DECRYPT_MODE, key1,iv);
        	byte[] plain = cipher1.doFinal(cipher);
        	assert1 = "The encryption is SUCCESSFUL!!!The old file is REMOVED,the new file is generated!";
        	assert2 = null;
        	return plain;
        }
        else{
        	assert1 = "The file is corrupted!! The MAC has been changed!!";
        	assert2 = ".8102";
        	return cipherText;
        }
		
	}
	else{
		assert1 = "The password is WRONG!!";
		assert2 = ".8102";
		return cipherText;
	}	
}
public String returnTheAssert(){
	return assert1;
}
public String returnTheAssert2(){
	if(assert2 == null){
		return "";
	}
	else{
		return ".8102";
	}
}


}
