package Mysafe;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;


import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.swing.*;


public class GUI extends JFrame {

/**
	 * 
	 */
	private static final long serialVersionUID = 7127977537858393091L;
JLabel jl1, jl2;
JButton queding, qingchu, xuanz, jiami, jiemi;// button "confirm", "Clear", "choose file" "Encrypt" "Decrypt"
JTextField lujin, passwordArea;//"path" 
JTextArea nr;
JRadioButton qu, xie;
ButtonGroup fz;// add the botton


File file;
File newFile;

String string;
String frameArea = null;
String plainText = null;
String cipherText = null;
byte[] MAC;
byte[] result;
byte bytearray[] = null;
Key key1, key2;

IvParameterSpec iv = null;

MySafe e1 = new MySafe();
public GUI() {
   Container c = getContentPane();//GUI Jframe
   JPanel jp1 = new JPanel();
   jl1 = new JLabel("Path");
   lujin = new JTextField(15);
   xuanz = new JButton("Choose File");
   jp1.add(jl1);
   jp1.add(lujin);
   jp1.add(xuanz);
   c.add(jp1, BorderLayout.NORTH);// add the botton


   nr = new JTextArea();
   c.add(new JScrollPane(nr), BorderLayout.CENTER);


   qu = new JRadioButton("Wrtie");
   xie = new JRadioButton("Read", true);
   fz = new ButtonGroup();
   fz.add(qu);
   fz.add(xie);// add the botton


   jl2 = new JLabel("password");
   passwordArea = new JTextField(15);
   jiami = new JButton("Encrypt");
   jiemi = new JButton("Decrypt");
   JPanel jp4 = new JPanel();
   jp4.setLayout(new GridLayout(2, 1, 5, 5));
   JPanel jp2 = new JPanel();
   jp2.add(jl2);
   jp2.add(passwordArea);
   jp2.add(jiami);
   jp2.add(jiemi);
   jp4.add(jp2);// add the botton


   JPanel jp3 = new JPanel();
   queding = new JButton("Confirm");
   qingchu = new JButton("Clear");
   jp3.add(qu);
   jp3.add(xie);
   jp3.add(queding);
   jp3.add(qingchu);
   jp4.add(jp3);// add the botton


   c.add(jp4, BorderLayout.SOUTH);
   queding.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent event) {
     
     if (qu.isSelected())//choose to read the file
      shuchu();
     if (xie.isSelected())// choose to write the file
      shuru();
    }
   });
   qingchu.addActionListener(new ActionListener() {
   public void actionPerformed(ActionEvent event) {
    nr.setText(""); // clear the path area
    passwordArea.setText("");// clear the password in the GUI
   }
  });
   xuanz.addActionListener(new ActionListener() {// to select the file
    public void actionPerformed(ActionEvent event) {
     JFileChooser fileChooser = new JFileChooser(); // initial the file dialog 
     fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES); // the choosing type of file
     if (fileChooser.showOpenDialog(GUI.this) == JFileChooser.APPROVE_OPTION) { // get the file dialog
      String fileName = fileChooser.getSelectedFile().getAbsolutePath(); // get the file path
      lujin.setText(fileName);
     }
    }
   });

   jiami.addActionListener(new ActionListener() // click the "Encrypt" this is Encryption
   {
    public void actionPerformed(ActionEvent event) 
    {
    string = passwordArea.getText();
    if (string.length() == 0)// the password can not be empty
	{
	nr.setText("Input the password!");
	}
    else if(lujin.getText().contains(".8102"))// choose the file without ecryption
    {
    	nr.setText("This is already a encryted file, you cannot encrypt agian!sorry");
    }
    else
    {
    	
    try
    { 
    	
        file=new File(lujin.getText()); // read the file with the pass in the pass Area
        FileInputStream fileinputstream = new FileInputStream(file);

        int numberBytes = fileinputstream.available();
        bytearray = new byte[numberBytes];

        fileinputstream.read(bytearray);
        fileinputstream.close();
        file.delete();//delete the fiel
        nr.setText(""); 
        if((bytearray.length!= 0))
        {
          byte[] out = e1.encrypt(string,bytearray);
         
        //Create the new file, Store the MAC, delete the old file.  
           newFile = new File(lujin.getText()+".8102");//create a new file with ".8102"
           DataOutputStream os = new DataOutputStream(new FileOutputStream(newFile));
           os.write(out);
           os.close();
           
           nr.setText("The encryption is successful!!! The old file is removed!!!");
        }
        else if ((bytearray.length== 0))
        {
        	nr.setText("No such file or file is empty!");
        	file.delete();
        }
     }
    catch (IOException e) {
    	nr.setText("The file is Empty or No such file!");
    } catch (InvalidKeyException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (NoSuchPaddingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InvalidAlgorithmParameterException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalBlockSizeException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (BadPaddingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
    }
}
   });

   jiemi.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent event) {
    string = passwordArea.getText();
    if (string.length() == 0)// the password should not be empty
    {
    	nr.setText("Input the password!");
    }
    
    else if(lujin.getText().indexOf(".8102") == -1){
    	nr.setText("This is already a decrypted file!!!");
    }
    else{
    try{
        file = new File(lujin.getText());// read the cipertexy in the file
        FileInputStream fileinputstream = new FileInputStream(file);

        int numberBytes = fileinputstream.available();
        byte bytearray[] = new byte[numberBytes];// define a new array with the same length of the data from the encrypted file

        fileinputstream.read(bytearray);
        fileinputstream.close();// close the input stram
        file.delete();
        nr.setText(""); 
        if((bytearray.length !=0))
        {
        	byte[] out = e1.decrypt(string, bytearray);//have not put parma!!
        	
          //Create the new file, Store the plaintext, delete the old file.  
            newFile = new File(lujin.getText().replace(".8102", "")+ e1.returnTheAssert2());// delete the ".8102"
            DataOutputStream os = new DataOutputStream(new FileOutputStream(newFile));
            os.write(out);
            os.close();
            
            nr.setText(e1.returnTheAssert());    
        }
        else if ((bytearray.length== 0))//the file can not be empty
        {
        	nr.setText("No such file or file is empty!");
        	file.delete();
        }
    }
    catch (IOException e) {
    	nr.setText("The file is Empty or No such file!");
    } catch (InvalidKeyException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (NoSuchPaddingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InvalidAlgorithmParameterException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalBlockSizeException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (BadPaddingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   }

    }
   });
   setSize(380, 350);
   setVisible(true);
}

public void shuchu() {
try {
   file = new File(lujin.getText());// read the plaintext from the file
   FileOutputStream out = new FileOutputStream(file);
   byte buf[] = nr.getText().getBytes();
   try {
    out.write(buf);
    out.flush();
    out.close();
   } catch (IOException e) {
    nr.setText("The file is Empty or No such file! ");
   }
  } catch (FileNotFoundException e) {
   nr.setText("The file is not Existed!!");
  }
}

public void shuru() {
   try {
    file = new File(lujin.getText());// read the file
    FileInputStream in = new FileInputStream(file);
    int a = (int) file.length();
    byte buf[] = new byte[a];
    try {
     int len = in.read(buf);// the file is not empty
     if (len == -1)
      System.out.println("The file is empty");
     else
      nr.setText(new String(buf, 0, len));
    } catch (IOException e) {
    	nr.setText("The file is Empty or No such file!");
    }
   } catch (FileNotFoundException e) {
	   nr.setText("The file is not Existed!!");
   }
}

}
